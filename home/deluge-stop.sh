#!/bin/bash

logger -s "Termino Deluged"
sudo /etc/init.d/deluge-daemon stop

pkill deluge-web
pkill deluged

exit 0