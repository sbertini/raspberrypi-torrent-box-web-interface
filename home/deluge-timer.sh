#!/bin/bash

. ./deluge-setvars.sh

RUNNING=`ps ax | grep deluged | grep -vc grep`

#echo Running: $RUNNING

python deluge-check-run.py $RUNNING > timer.log

STATUS=$?

if [ "$DELUGE_CGI_CONFIG" != "" ]; then

	. $DELUGE_CGI_CONFIG
	
	if [ -f $FLAGS_FILE_START ] 
	then
		if [ $RUNNING -eq 0 ]
		then
			logger -s "Avvio forzato"
			STATUS=1
		else 
			STATUS=3
		fi
	elif [ -f $FLAGS_FILE_STOP ]
	then
		if [ $RUNNING -eq 1 ]
		then
			logger -s "Stop forzato"
			STATUS=2
		else 
			STATUS=3
		fi
	fi

fi

logger -s "Timer deluged: Running: $RUNNING, Status: $STATUS"


if [ $STATUS -eq 1 ]
then
	logger -s "Avvio il demone deluged"
	./deluge-start.sh
	exit 0
fi

if [ $STATUS -eq 2 ] 
then
	logger -s "Termino il demone deluged"
	./deluge-stop.sh
	exit 0
fi



