#!/bin/bash

#Imposto i parametri per il mount
. ./deluge-setvars.sh

# Verifica se montare o meno la condivisione
mount -t cifs | grep -q storage 
if [ $? -eq 0 ]
then
	logger -s "Share dati per Deluged già montato"
else
	logger -s "Monto lo share dati per Deluged"
	sudo mount -t cifs -o username=$SMB_USERNAME,password=$SMB_PASSWORD,rw,file_mode=0777,dir_mode=0777 $SMB_SHARE /mnt/storage
	
	mount -t cifs | grep -q storage 
	if [ $? -ne 0 ]
	then
		logger -s "Errore tentando di montare lo share dati per Deluged"
		exit -1
	fi
fi

logger -s "Avvio Deluged"
sudo /etc/init.d/deluge-daemon start

exit 0