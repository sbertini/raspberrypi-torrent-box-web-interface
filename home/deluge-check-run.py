import time, sys

debug = False
#debug = True

delugedRunning = False
  
if len(sys.argv) > 1:
  tmp = sys.argv[1]
  if tmp == '1':
    delugedRunning = True

if debug:
  print 'Deluged is already running ? {}'.format(delugedRunning) 

with open ("deluge-times.cfg", "r") as myfile:
  data=myfile.readlines()
    
shouldStart = False

for item in data:

  if debug:
    print 'Read line from file  {}'.format(item) 
  
  if item.strip()[0] == '#':
  	continue

  tokens = item.rstrip().split (',' ,3)
  item_dow = tokens[0]
  item_start = tokens[1]
  item_stop = tokens[2]
    	
  now = time.strftime("%H:%M:%S")
  dow = time.strftime("%w")
      
  if debug:
    print 'Days of week {}'.format(item_dow) 
    print 'Start hour   {}'.format(item_start) 
    print 'End hour     {}'.format(item_stop) 
    print 'Now is       {}'.format(now) 
    print 'Today dow is {}'.format(dow) 
  
  dayOk = False
  startOk = False
  stopOk  = False
  
  if item_dow.find(dow) !=-1:
    dayOk = True

  if now >= item_start:
    startOk = True

  if now <= item_stop:
    stopOk = True

  if debug:
    print 'Day Ok: {} Start Ok: {} End Ok: {}'.format(dayOk, startOk, stopOk) 
  
  shouldStartForEntry = dayOk and startOk and stopOk 
  shouldStart = shouldStart or shouldStartForEntry

  if debug:
    print 'shouldStartForEntry: {} shouldStart: {}'.format(shouldStartForEntry, shouldStart) 
    print

if shouldStart and not delugedRunning:
  if debug:
    print "Start"
  sys.exit(1)
elif not shouldStart and delugedRunning:
  if debug:
    print "Stop"
  sys.exit(2)
else :
  if debug:
    print "Do nothing"
  sys.exit(0)
