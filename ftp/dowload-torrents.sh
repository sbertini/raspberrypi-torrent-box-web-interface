#!/bin/sh

# http://www.stratigery.com/scripting.ftp.html

. ./dowload-torrents-config.sh

ftp -n $FTP_HOST <<END_SCRIPT
quote USER $FTP_USER
quote PASS $FTP_PASSWD
binary
prompt off
cd $FTP_FOLDER
lcd $LOCAL_FOLDER
mget *
mdelete *
quit
END_SCRIPT

exit 0