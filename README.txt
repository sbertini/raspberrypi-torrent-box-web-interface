Setup del sistema:

1) Installare il client Samba con i seguenti comandi

sudo apt-get update
sudo apt-get upgrade
sudo apt-get install smbclient

sudo mkdir /mnt/storage

2) Installare Deluge

Seguire completamente questo tutorial (http://www.howtogeek.com/142044/how-to-turn-a-raspberry-pi-into-an-always-on-bittorrent-box/) 

Al termine, configurare Deluge in modo che vada a leggere/scrivere in una sottodirectory dello share samba montato (/mnt/storage/....)

3) Installare gli script

Copiare gli script del folder HOME nella home dell'utente PI.

Configurare le impostazioni dello share samba nel file deluge-setvars.sh

Configurare i giorni e gli orari di start-stop nel file deluge-times.cfg

Aggiungere la schedulazione dello script di controllo con il comando

crontab -e

inserendo la riga seguente

*/1 * * * * /home/pi/deluge-timer.sh

4) Installare apache

(http://elinux.org/RPi_Apache2)

# Create the www-data group
sudo addgroup www-data
sudo adduser -m www-data www-data

#Create the www-data home folder
cd /home
sudo mkdir www-data
sudo chown www-data.www-data www-data

# Create the /var/www folder and reassign ownership of the folder (and any files/subfolders)
sudo mkdir /var/www
sudo chown -R www-data:www-data /var/www

sudo apt-get update
sudo apt-get upgrade
sudo apt-get install apache2

#Copiare i files del modulo CGI-BIN su /tmp e poi eseguire
cd /usr/lib/cgi-bin
sudo mv /tmp/deluge* .
sudo chown root.root deluge*
sudo chmod a+x deluge*

#Copiare la pagina index.html su /tmp e poi eseguire
cd /var/www
sudo mv /tmp/index.html .
sudo chown www-data.www-data index.html

#Aggiungere i permessi a sudoers
sudo visudo

Aggiungere in fondo la riga
www-data ALL=(ALL) NOPASSWD: /sbin/halt,/sbin/reboot


5) Gestione scaricamento automatico nuovi torrents da FTP

Installare il client FTP con i seguenti comandi

sudo apt-get update
sudo apt-get upgrade
sudo apt-get install ftp

Configurare opportunamente il file download-torrents-config.sh 
Il folder specificato dal parametro LOCAL_FOLDER deve corrispondere a quello configurato i Deluge come "AutoAdd folder"

Aggiungere la schedulazione dello script di controllo con il comando

crontab -e

inserendo la riga seguente

*/30 * * * * /home/pi/download-torrents.sh
