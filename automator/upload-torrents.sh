#!/bin/sh

FTP_HOST=''
FTP_USER=''
FTP_PASSWD=''
FTP_FOLDER=''

DATA=`cat`

FTP_BATCH=
FTP_BATCH+="quote USER $FTP_USER"
FTP_BATCH+=$'\n'
FTP_BATCH+="quote PASS $FTP_PASSWD"
FTP_BATCH+=$'\n'
FTP_BATCH+="binary"
FTP_BATCH+=$'\n'
FTP_BATCH+="prompt off"
FTP_BATCH+=$'\n'
FTP_BATCH+="cd $FTP_FOLDER"
FTP_BATCH+=$'\n'


while read ENTRY ; do
	if [ -f "$ENTRY" ]
	then
	  NAME=`basename "$ENTRY"`
	  FTP_BATCH+="put \"$ENTRY\" \"$NAME\""
	  FTP_BATCH+=$'\n'
 	fi
done < <(echo "$DATA")

FTP_BATCH+="quit"
FTP_BATCH+=$'\n'

ftp -n $FTP_HOST <<< "$FTP_BATCH" > /tmp/ftp.log.$$

egrep -i "not found|No such|refused|failed|error|timed out" /tmp/ftp.log.$$ > /dev/null 
if [ $? -eq 0 -o $? -eq 2 ] ; then 
	ERROR=1 
	cat "/tmp/ftp.log.$$" # to see the errors 
else 
	ERROR=0 
	rm -f /tmp/ftp.log.$$ 
fi 

echo ERROR $ERROR

if [ $ERROR -eq 0 ]
then
	while read ENTRY ; do
		if [ -f "$ENTRY" ]
		then
		  BACKUP_FOLDER=`dirname "$ENTRY"`
		  mv -f "$ENTRY" "$BACKUP_FOLDER/_Done"
		fi
	done < <(echo "$DATA")
fi